package es.unex.navdrawertraining;

import org.junit.Test;

import java.lang.reflect.Field;
import static org.junit.Assert.assertEquals;

import es.unex.navdrawertraining.data.modelodatos.Favorito;

public class FavoritoUnitTest {

    @Test
    public void setId_favorito_test() throws NoSuchFieldException, IllegalAccessException{
        long value = 123;
        long value2 = 128;
        Favorito instance = new Favorito(value);
        instance.setId_favorito(value2);
        final Field field = instance.getClass().getDeclaredField("id_favorito");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value2);

    }

    @Test
    public void getId_favorito_test()throws NoSuchFieldException, IllegalAccessException{
        long value = 123;
        Favorito instance = new Favorito(value);
        final Field field = instance.getClass().getDeclaredField("id_favorito");
        field.setAccessible(true);
        field.set(instance, 128);

        //when
        final long result = instance.getId_favorito();

        //then
        assertEquals("field wasn't retrieved properly", result, 128);
    }

}
