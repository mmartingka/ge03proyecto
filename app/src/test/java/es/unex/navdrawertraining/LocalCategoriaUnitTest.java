package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.navdrawertraining.data.modelodatos.LocalCategoria;

public class LocalCategoriaUnitTest {
    @Test
    public void setId_categoriaTest () throws NoSuchFieldException, IllegalAccessException {
        long value = 123L;
        LocalCategoria instance = new LocalCategoria(-1, -1);
        instance.setId_categoria(value);
        final Field field = instance.getClass().getDeclaredField("id_categoria");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setId_localTest () throws NoSuchFieldException, IllegalAccessException {
        long value = 321L;
        LocalCategoria instance = new LocalCategoria(-1, -1);
        instance.setId_local(value);
        final Field field = instance.getClass().getDeclaredField("id_local");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
    @Test
    public void getId_categoriaTest () throws NoSuchFieldException, IllegalAccessException {
        final LocalCategoria instance = new LocalCategoria(-1, -1);
        final Field field = instance.getClass().getDeclaredField("id_categoria");
        field.setAccessible(true);
        field.set(instance, 123L);

        //when
        final long result = instance.getId_categoria();

        //then
        assertEquals("field wasn't retrieved properly", result, 123L);
    }
    @Test
    public void getId_localTest () throws NoSuchFieldException, IllegalAccessException {
        final LocalCategoria instance = new LocalCategoria(-1, -1);
        final Field field = instance.getClass().getDeclaredField("id_local");
        field.setAccessible(true);
        field.set(instance, 321L);

        //when
        final long result = instance.getId_local();

        //then
        assertEquals("field wasn't retrieved properly", result, 321L);
    }
}
