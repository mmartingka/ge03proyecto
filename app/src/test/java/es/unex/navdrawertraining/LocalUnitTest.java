package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Local;


public class LocalUnitTest {

    //Pruebas de métodos set
    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
        long value = 123;
        Local instance = new Local();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setNombreTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Atrio";
        Local instance = new Local();
        instance.setNombre(value);
        final Field field = instance.getClass().getDeclaredField("nombre");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setDescripcionTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Muy caro, buena calidad";
        Local instance = new Local();
        instance.setDescripcion(value);
        final Field field = instance.getClass().getDeclaredField("descripcion");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setCoordenadasTest() throws NoSuchFieldException, IllegalAccessException {
        List<Double> values = new ArrayList<Double>();
        values.add(3.6589);
        values.add(2.1234);
        Local instance = new Local();
        instance.setCoordenadas(values);
        final Field field = instance.getClass().getDeclaredField("coordenadas");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), values);
    }

    @Test
    public void setValoracionGlobalTest() throws  NoSuchFieldException, IllegalAccessException {
        float value = 4.5f;
        Local instance = new Local();
        instance.setValoracionGlobal(value);
        final Field field = instance.getClass().getDeclaredField("valoracionGlobal");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setFotoPerfilTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "fotoDePerfil";
        Local instance = new Local();
        instance.setFotoPerfil(value);
        final Field field = instance.getClass().getDeclaredField("fotoPerfil");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setFotoBackgroundTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "fotodeBackground";
        Local instance = new Local();
        instance.setFotoBackground(value);
        final Field field = instance.getClass().getDeclaredField("fotoBackground");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setAforoTotalTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 100;
        Local instance = new Local();
        instance.setAforoTotal(value);
        final Field field = instance.getClass().getDeclaredField("aforoTotal");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
    @Test
    public void setOcupacionActualTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 50;
        Local instance = new Local();
        instance.setOcupacionActual(value);
        final Field field = instance.getClass().getDeclaredField("ocupacionActual");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
    @Test
    public void setEtiquetasTest() throws NoSuchFieldException, IllegalAccessException {
        List<String> values = new ArrayList<String>();
        values.add("Cañas");
        values.add("Hamburguesas");
        values.add("Karaoke");
        Local instance = new Local();
        instance.setEtiquetas(values);
        final Field field = instance.getClass().getDeclaredField("etiquetas");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), values);
    }
    //Pruebas de métodos get
    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 128L);

        //when
        final long result = instance.getId();

        //then
        assertEquals("field wasn't retrieved properly", result, 128);
    }

    @Test
    public void getNombreTest() throws NoSuchFieldException, IllegalAccessException {
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("nombre");
        field.setAccessible(true);
        field.set(instance, "Atrio");

        //when
        String result = instance.getNombre();

        //then
        assertEquals("field wasn't retrieved properly", result, "Atrio");
    }

    @Test
    public void getDescripcionTest() throws NoSuchFieldException, IllegalAccessException {
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("descripcion");
        field.setAccessible(true);
        field.set(instance, "Buena calidad");

        //when
        String result = instance.getDescripcion();

        //then
        assertEquals("field wasn't retrieved properly", result, "Buena calidad");
    }

    @Test
    public void getCoordenadasTest() throws NoSuchFieldException, IllegalAccessException {
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("coordenadas");
        field.setAccessible(true);
        List<Double> coord = new ArrayList<>();
        coord.add(5.34);
        coord.add(4.32);

        field.set(instance, coord);

        //when
        List<Double> result = instance.getCoordenadas();

        //then
        assertEquals("field wasn't retrieved properly", result, coord);
    }

    @Test
    public void getValoracionGlobalTest()throws NoSuchFieldException, IllegalAccessException{
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("valoracionGlobal");
        field.setAccessible(true);
        field.set(instance, 4.2F);

        //when
        float result = instance.getValoracionGlobal();

        //then
        assertEquals("field wasn't retrieved properly", result, 4.2F, 0.001F);
    }

    @Test
    public void getFotoPerfilTest() throws NoSuchFieldException, IllegalAccessException {
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("fotoPerfil");
        field.setAccessible(true);
        field.set(instance, "Imagen");

        //when
        String result = instance.getFotoPerfil();

        //then
        assertEquals("field wasn't retrieved properly", result, "Imagen");
    }

    @Test
    public void getFotoBackgroundTest() throws NoSuchFieldException, IllegalAccessException {
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("fotoBackground");
        field.setAccessible(true);
        field.set(instance, "Imagen background");

        //when
        String result = instance.getFotoBackground();

        //then
        assertEquals("field wasn't retrieved properly", result, "Imagen background");
    }

    @Test
    public void getAforoTotalTest()throws NoSuchFieldException, IllegalAccessException{
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("aforoTotal");
        field.setAccessible(true);
        field.set(instance, 100);

        //when
        int result = instance.getAforoTotal();

        //then
        assertEquals("field wasn't retrieved properly", result, 100);
    }

    @Test
    public void getOcupacionActualTest()throws NoSuchFieldException, IllegalAccessException{
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("ocupacionActual");
        field.setAccessible(true);
        field.set(instance, 40);

        //when
        int result = instance.getOcupacionActual();

        //then
        assertEquals("field wasn't retrieved properly", result, 40);
    }

    @Test
    public void getEtiquetasTest() throws NoSuchFieldException, IllegalAccessException {
        final Local instance = new Local();
        final Field field = instance.getClass().getDeclaredField("etiquetas");
        field.setAccessible(true);
        List<String> tags = new ArrayList<String>();
        tags.add("Cafetería");
        tags.add("Rastaurante");

        field.set(instance, tags);

        //when
        List<String> result = instance.getEtiquetas();

        //then
        assertEquals("field wasn't retrieved properly", result, tags);
    }

    @Test
    public void compareToTest () throws NoSuchFieldException, IllegalAccessException{
        final Local instance = new Local();
        final Local instance2 = new Local();

        final Field valoracionGlobal = Local.class.getDeclaredField("valoracionGlobal");
        valoracionGlobal.setAccessible(true);
        valoracionGlobal.set(instance, 4.5f);
        valoracionGlobal.set(instance2, 4.5f);

        final Field aforoTotal = Local.class.getDeclaredField("aforoTotal");
        aforoTotal.setAccessible(true);
        aforoTotal.set(instance, 100);
        aforoTotal.set(instance2, 100);

        final Field ocupacionActual = Local.class.getDeclaredField("ocupacionActual");
        ocupacionActual.setAccessible(true);
        ocupacionActual.set(instance, 50);
        ocupacionActual.set(instance2, 20);

        assertEquals("The comparation went wrong", instance.compareTo(instance2), 1);

        ocupacionActual.set(instance2, 70);
        assertEquals("The comparation went wrong", instance.compareTo(instance2), -1);

        ocupacionActual.set(instance2, 50);
        assertEquals("The comparation went wrong", instance.compareTo(instance2), 0);

        valoracionGlobal.set(instance2, 4F);
        assertEquals("The comparation went wrong", instance.compareTo(instance2), -1);

        valoracionGlobal.set(instance2, 5F);
        assertEquals("The comparation went wrong", instance.compareTo(instance2), 1);



    }


}
