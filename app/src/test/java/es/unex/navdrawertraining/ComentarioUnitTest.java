package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.navdrawertraining.data.modelodatos.Comentario;

public class ComentarioUnitTest {
    @Test
    public void setId_comentarioTest() throws NoSuchFieldException, IllegalAccessException{
        Comentario instance = new Comentario(-1,"");
        long value=5;
        instance.setId_comentario(value);

        final Field field = instance.getClass().getDeclaredField("id_comentario");
        field.setAccessible(true);

        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setCadena_comentarioTest() throws NoSuchFieldException, IllegalAccessException{
        Comentario instance=new Comentario(-1,"");
        String value="Esto es un comentario.";
        instance.setCadena_comentario(value);

        final Field field = instance.getClass().getDeclaredField("cadena_comentario");
        field.setAccessible(true);

        assertEquals("Fields didn't match", field.get(instance), value);
    }

    //Pruebas de métodos get
    @Test
    public void getId_comentarioTest() throws NoSuchFieldException, IllegalAccessException {
        Comentario instance = new Comentario(8,"Hola");
        final Field field = instance.getClass().getDeclaredField("id_comentario");
        field.setAccessible(true);
        field.set(instance, 128);

        //when
        final long result = instance.getId_comentario();

        //then
        assertEquals("field wasn't retrieved properly", result, 128);
    }

    @Test
    public void getCadena_comentarioTest() throws NoSuchFieldException, IllegalAccessException {
        Comentario instance = new Comentario(8,"Hola");
        final Field field = instance.getClass().getDeclaredField("cadena_comentario");
        field.setAccessible(true);
        field.set(instance, "Esto es un comentario");

        //when
        final String result = instance.getCadena_comentario();

        //then
        assertEquals("field wasn't retrieved properly", result, "Esto es un comentario");
    }


}