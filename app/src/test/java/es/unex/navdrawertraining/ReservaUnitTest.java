package es.unex.navdrawertraining;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;

import es.unex.navdrawertraining.data.modelodatos.Reserva;


public class ReservaUnitTest {

    @Test
    public void setId_reservaTest() throws NoSuchFieldException, IllegalAccessException {
        long value = 10;
        Reserva instance = new Reserva(-1, new Date());
        instance.setId_reserva(value);
        final Field field =  instance.getClass().getDeclaredField("id_reserva");
        field.setAccessible(true);
        Assert.assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getId_reservaTest() throws NoSuchFieldException, IllegalAccessException {
        final Reserva instance = new Reserva(-1, new Date());
        final Field field = instance.getClass().getDeclaredField("id_reserva");
        field.setAccessible(true);
        field.set(instance, 10);

        //when
        final long result = instance.getId_reserva();
        //then
        Assert.assertEquals("Field wasn't retrieved properly", result, 10);
    }

    @Test
    public void setFecha_hora_reservaTest() throws NoSuchFieldException, IllegalAccessException {
        Date value = new Calendar.Builder().setDate(2021, 12, 25).build().getTime();
        Reserva instance = new Reserva(-1, new Date());
        instance.setFecha_hora_reserva(value);
        final Field field =  instance.getClass().getDeclaredField("fecha_hora_reserva");
        field.setAccessible(true);
        Assert.assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getFecha_hora_reservaTest() throws NoSuchFieldException, IllegalAccessException {
        final Reserva instance = new Reserva(-1, new Date());
        final Field field = instance.getClass().getDeclaredField("fecha_hora_reserva");
        field.setAccessible(true);
        field.set(instance, new Calendar.Builder().setDate(2021, 12, 24).build().getTime());

        //when
        final Date result = instance.getFecha_hora_reserva();
        //then
        Assert.assertEquals("Field wasn't retrieved properly", result, new Calendar.Builder().setDate(2021, 12, 24).build().getTime());
    }
}

