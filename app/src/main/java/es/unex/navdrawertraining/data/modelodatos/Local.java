package es.unex.navdrawertraining.data.modelodatos;

import android.os.Bundle;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import es.unex.navdrawertraining.data.roomdb.CoordenadasConverter;
import es.unex.navdrawertraining.data.roomdb.EtiquetasConverter;

@Entity(tableName = "local")
public class Local implements Comparable<Local>{
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @TypeConverters(CoordenadasConverter.class)
    @SerializedName("coordenadas")
    @Expose
    private List<Double> coordenadas = null;
    @SerializedName("valoracionGlobal")
    @Expose
    private float valoracionGlobal;
    @SerializedName("foto_perfil")
    @Expose
    private String fotoPerfil;
    @SerializedName("foto_background")
    @Expose
    private String fotoBackground;
    @SerializedName("aforo_total")
    @Expose
    private Integer aforoTotal;
    @SerializedName("ocupacion_actual")
    @Expose
    private Integer ocupacionActual;
    @TypeConverters(EtiquetasConverter.class)
    @SerializedName("etiquetas")
    @Expose
    private List<String> etiquetas = null;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Double> getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(List<Double> coordenadas) {
        this.coordenadas = coordenadas;
    }

    public float getValoracionGlobal() {
        return valoracionGlobal;
    }

    public void setValoracionGlobal(float valoracionGlobal) {
        this.valoracionGlobal = valoracionGlobal;
    }

    public String getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(String fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public String getFotoBackground() {
        return fotoBackground;
    }

    public void setFotoBackground(String fotoBackground) {
        this.fotoBackground = fotoBackground;
    }

    public Integer getAforoTotal() {
        return aforoTotal;
    }

    public void setAforoTotal(Integer aforoTotal) {
        this.aforoTotal = aforoTotal;
    }

    public Integer getOcupacionActual() {
        return ocupacionActual;
    }

    public void setOcupacionActual(Integer ocupacionActual) {
        this.ocupacionActual = ocupacionActual;
    }

    public List<String> getEtiquetas() {
        return etiquetas;
    }

    public void setEtiquetas(List<String> etiquetas) {
        this.etiquetas = etiquetas;
    }

    public Bundle bundlelizeLocal () {
        Bundle bundle = new Bundle();
        bundle.putLong("id", id);
        bundle.putString("nombre", nombre);
        bundle.putString("descripcion", descripcion);
        double[] coordenadasArray = {coordenadas.get(0), coordenadas.get(1)};
        bundle.putDoubleArray("coordenadas", coordenadasArray);
        bundle.putFloat("valoracion", valoracionGlobal);
        bundle.putString("fotoPerfil", fotoPerfil);
        bundle.putString("fotoBackground", fotoBackground);
        bundle.putInt("aforoTotal", aforoTotal);
        bundle.putInt("ocupacionActual", ocupacionActual);
        bundle.putStringArrayList("tags", new ArrayList<String>(etiquetas));
        return bundle;
    }

    @Override
    public int compareTo(Local local) {
        if (valoracionGlobal == local.getValoracionGlobal()) {
            if (((float) ocupacionActual / aforoTotal)
                    == ((float) local.getOcupacionActual() / local.getAforoTotal())) {
                return 0;
            } else {
                if (( ocupacionActual * 1.0f / aforoTotal)
                        > (local.getOcupacionActual() * 1.0f / local.getAforoTotal())) {
                    return 1;
                } else {
                    return -1;
                }
            }
        } else {
            if (valoracionGlobal > local.getValoracionGlobal()) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}