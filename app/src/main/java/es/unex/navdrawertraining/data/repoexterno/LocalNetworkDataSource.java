package es.unex.navdrawertraining.data.repoexterno;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import es.unex.navdrawertraining.AppExecutors;
import es.unex.navdrawertraining.data.modelodatos.Local;

public class LocalNetworkDataSource {

    //Instancia del Singleton
    private static LocalNetworkDataSource sInstance;

    //LiveData storing the latest downloaded locales
    private final MutableLiveData<List<Local>> mDonloadedLocales;

    //Tag for logcat
    private static final String LOG_TAG = LocalNetworkDataSource.class.getSimpleName();

    private LocalNetworkDataSource() {
        mDonloadedLocales = new MutableLiveData<>();
    }

    public synchronized static LocalNetworkDataSource getInstance() {
        Log.i(LOG_TAG, "Getting the instance of LocalNetworkDataSource");
        if (sInstance == null) {
            sInstance = new LocalNetworkDataSource();
            Log.i(LOG_TAG, "No instance exists, so make one.");
        }
        return sInstance;
    }

    public LiveData<List<Local>> getCurrentLocales () {
        Log.i(LOG_TAG, "Retrieving downloaded locales");
        return mDonloadedLocales;
    }

    public void fetchLocales () {
        Log.i(LOG_TAG, "Downloading from the API the locales");
        AppExecutors.getInstance().networkIO().execute(new LocalesNetworkLoaderRunnable(locales -> mDonloadedLocales.postValue(locales)));
    }
}
