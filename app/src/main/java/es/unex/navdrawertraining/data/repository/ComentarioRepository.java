package es.unex.navdrawertraining.data.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import es.unex.navdrawertraining.data.modelodatos.Comentario;
import es.unex.navdrawertraining.data.roomdb.ComentarioDao;

public class ComentarioRepository {
    private static final String LOG_TAG = ComentarioRepository.class.getSimpleName();

    private static ComentarioRepository sInstance;
    private final ComentarioDao mComentarioDao;
    private final MutableLiveData<Long> mComentarioLiveData = new MutableLiveData<>();

    private ComentarioRepository (ComentarioDao comentarioDao) {
        mComentarioDao = comentarioDao;
    }

    public synchronized static ComentarioRepository getInstance(ComentarioDao comentarioDao) {
        Log.i(LOG_TAG, "Getting the ComentarioRepository");
        if (sInstance == null) {
            sInstance = new ComentarioRepository(comentarioDao);
            Log.i(LOG_TAG, "Made the instance of the Comentario Repository");
        }
        return sInstance;
    }

    public void setLocalId (long id) {
        mComentarioLiveData.setValue(id);
    }

    public LiveData<Comentario> getComentario () {
        Log.i(LOG_TAG, "Fetching Comentario from data base");
        return Transformations.switchMap(mComentarioLiveData, idLocal -> mComentarioDao.get(idLocal));
    }
}
