package es.unex.navdrawertraining.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Reserva;
import es.unex.navdrawertraining.helpers.LocalReservaAggregate;

@Dao
public interface ReservaDao {
    @Query("SELECT * FROM Reserva r INNER JOIN Local l ON (r.id_reserva = l.id)")
    public LiveData<List<LocalReservaAggregate>> getAll();

    @Query("SELECT * FROM Reserva WHERE id_reserva= :reserva_id AND fecha_hora_reserva= :fecha_hora")
    public Reserva get(long reserva_id, Long fecha_hora);

    @Insert
    public long insert(Reserva reserva);

    @Query("DELETE FROM Reserva WHERE id_reserva = :id AND fecha_hora_reserva = :fecha_hora_reserva")
    public void delete(long id, Long fecha_hora_reserva);
}
