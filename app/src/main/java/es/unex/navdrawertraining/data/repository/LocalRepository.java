package es.unex.navdrawertraining.data.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.navdrawertraining.AppExecutors;
import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.repoexterno.LocalNetworkDataSource;
import es.unex.navdrawertraining.data.roomdb.LocalDao;

public class LocalRepository {

    private static final String LOG_TAG = LocalRepository.class.getSimpleName();

    //For singleton instantiation
    private static LocalRepository sInstance;
    private final LocalDao mLocalDao;
    private final LocalNetworkDataSource mLocalNetworkDataSource;
    private final AppExecutors mAppExecutors = AppExecutors.getInstance();
    private final LiveData<List<Local>> mLocalesLiveList;
    private long lastFetchTime;
    private static final long TIME_LIMIT_FETCH = 30000;

    private LocalRepository (LocalDao localDao, LocalNetworkDataSource localNetworkDataSource){
        mLocalDao = localDao;
        mLocalNetworkDataSource = localNetworkDataSource;

        LiveData<List<Local>> localesLiveData = mLocalNetworkDataSource.getCurrentLocales();
        localesLiveData.observeForever(newLocalesFromNetwork -> mAppExecutors.diskIO().execute(() -> {
            if (newLocalesFromNetwork.size() > 0) {
                mLocalDao.deleteLocales();
                Log.i(LOG_TAG, "Old locales removed form Room");
            }
            mLocalDao.bulkInsert(newLocalesFromNetwork);
            Log.i(LOG_TAG, "New locales inserted in Room (" + newLocalesFromNetwork.size() + ")");
        }));
        mLocalesLiveList = mLocalDao.getLocales();
    }

    public synchronized static LocalRepository getInstance(LocalDao dao, LocalNetworkDataSource source) {
        Log.i(LOG_TAG, "Getting the LocalRepository");
        if (sInstance == null) {
            sInstance = new LocalRepository(dao, source);
            Log.i(LOG_TAG, "Made the instance of the Local Repository");
        }
        return sInstance;
    }

    public void doFetchLocales () {
        AppExecutors.getInstance().diskIO().execute(() -> {
            Log.i(LOG_TAG, "Fetching locales from github");
            mLocalDao.deleteLocales();
            mLocalNetworkDataSource.fetchLocales();
            lastFetchTime = System.currentTimeMillis();
        });

    }
    public LiveData<List<Local>> getCurrentLocales () {
        mAppExecutors.diskIO().execute(() -> {if (isFetchNeeded()) doFetchLocales();});
        return mLocalesLiveList;
    }

    private boolean isFetchNeeded () {
        return mLocalDao.getNumLocales() == 0 || TIME_LIMIT_FETCH <= (System.currentTimeMillis() - lastFetchTime);
    }
}
