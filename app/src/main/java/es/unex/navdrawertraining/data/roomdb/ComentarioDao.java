package es.unex.navdrawertraining.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import es.unex.navdrawertraining.data.modelodatos.Comentario;

@Dao
public interface ComentarioDao {
    @Query("SELECT * FROM comentario WHERE id_comentario = :id")
    public LiveData<Comentario> get(long id);

    @Insert
    public long insert(Comentario comentario);

    @Update
    public int update(Comentario comentario);
}
