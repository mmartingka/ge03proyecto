package es.unex.navdrawertraining.data.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.roomdb.LocalCategoriaDao;

public class LocalCategoriaRepository {
    private static final String LOG_TAG = LocalCategoriaRepository.class.getSimpleName();

    private static LocalCategoriaRepository sInstance;
    private final LocalCategoriaDao mLocalCategoriaDao;
    private final MutableLiveData<Long> mLocalCategoriaLiveData = new MutableLiveData<>();

    private LocalCategoriaRepository(LocalCategoriaDao mLocalCategoriaDao) {
        this.mLocalCategoriaDao = mLocalCategoriaDao;
    }

    public synchronized static LocalCategoriaRepository getInstance (LocalCategoriaDao mLocalCategoriaDao) {
        Log.i(LOG_TAG, "Getting the LocalCategoriaRepository");
        if (sInstance == null) {
            Log.i(LOG_TAG, "Made the instance of the LocalCategoria Repository");
            sInstance = new LocalCategoriaRepository(mLocalCategoriaDao);
        }
        return sInstance;
    }

    public void setCategoriaId (long categoriaId) {
        mLocalCategoriaLiveData.setValue(categoriaId);
    }

    public LiveData<List<Local>> getLocalesCategoria () {
        return Transformations.switchMap(mLocalCategoriaLiveData, categoriaId -> mLocalCategoriaDao.getLocalesCategoria(categoriaId));
    }
}
