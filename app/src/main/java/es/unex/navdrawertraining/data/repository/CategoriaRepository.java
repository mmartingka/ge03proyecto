package es.unex.navdrawertraining.data.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Categoria;
import es.unex.navdrawertraining.data.roomdb.CategoriaDao;

public class CategoriaRepository {
    private static final String LOG_TAG = CategoriaRepository.class.getSimpleName();
    private static CategoriaRepository sInstance;
    private final LiveData<List<Categoria>> mCategoriasLiveList;

    private CategoriaRepository (CategoriaDao categoriaDao) {
        Log.i(LOG_TAG, "Fetching the categories from the database");
        mCategoriasLiveList = categoriaDao.getAll();

    }

    public synchronized static CategoriaRepository getsInstance (CategoriaDao categoriaDao) {
        Log.i(LOG_TAG, "Getting the CategoriaRepository");
        if (sInstance == null) {
            sInstance = new CategoriaRepository(categoriaDao);
            Log.i(LOG_TAG, "Made the instance of the Categorias Repository");
        }
        return sInstance;
    }

    public LiveData<List<Categoria>> getCategorias () {
        return mCategoriasLiveList;
    }
}
