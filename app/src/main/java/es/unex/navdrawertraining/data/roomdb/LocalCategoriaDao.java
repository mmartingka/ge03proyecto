package es.unex.navdrawertraining.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.modelodatos.LocalCategoria;

@Dao
public interface LocalCategoriaDao {
    @Query("SELECT * FROM LocalCategoria WHERE id_categoria= :id_categoria AND id_local = :id_local")
    public LocalCategoria get (long id_categoria, long id_local);

    @Query("SELECT l.* FROM LocalCategoria lc JOIN Local l ON ( lc.id_local = l.id) WHERE id_categoria= :id_categoria")
    public LiveData<List<Local>> getLocalesCategoria (long id_categoria);

    @Insert
    public long insert(LocalCategoria localCategoria);

    @Query("DELETE FROM LocalCategoria WHERE  id_categoria= :id_categoria AND id_local = :id_local")
    public void delete(long id_categoria, long id_local);
}
