package es.unex.navdrawertraining.data.modelodatos;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.TypeConverters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import es.unex.navdrawertraining.data.roomdb.DateConverter;

@Entity(tableName = "reserva", primaryKeys = {"id_reserva", "fecha_hora_reserva"})
public class Reserva {

    @Ignore
    public final static String ID_RESERVA = "id_reserva";

    @Ignore
    public final static String FECHA_HORA_RESERVA = "fecha_hora_reserva";

    @Ignore
    public final static SimpleDateFormat FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss", Locale.US);

    @NonNull
    private long id_reserva;

    @TypeConverters(DateConverter.class)
    @NonNull
    private Date fecha_hora_reserva;

    public Reserva(long id_reserva, Date fecha_hora_reserva) {
        this.id_reserva = id_reserva;
        this.fecha_hora_reserva = fecha_hora_reserva;
    }

    public long getId_reserva() {
        return id_reserva;
    }

    public void setId_reserva(long id_reserva) {
        this.id_reserva = id_reserva;
    }

    public Date getFecha_hora_reserva() {
        return fecha_hora_reserva;
    }

    public void setFecha_hora_reserva(Date fecha_hora_reserva) {
        this.fecha_hora_reserva = fecha_hora_reserva;
    }
}
