package es.unex.navdrawertraining.data.roomdb;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Local;
@Dao
public interface LocalDao {

    @Insert(onConflict = REPLACE)
    public void bulkInsert(List<Local> locales);

    @Query("SELECT * FROM local")
    public LiveData<List<Local>> getLocales();

    @Query("SELECT COUNT(*) FROM local")
    public int getNumLocales();

    @Query("DELETE FROM local")
    public int deleteLocales();

}
