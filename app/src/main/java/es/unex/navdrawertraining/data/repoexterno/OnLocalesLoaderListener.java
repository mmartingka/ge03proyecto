package es.unex.navdrawertraining.data.repoexterno;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Local;

public interface OnLocalesLoaderListener {
    public void onLocalesLoaded (List<Local> locales);
}
