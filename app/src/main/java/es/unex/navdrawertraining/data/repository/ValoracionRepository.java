package es.unex.navdrawertraining.data.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import es.unex.navdrawertraining.data.modelodatos.Valoracion;
import es.unex.navdrawertraining.data.roomdb.ValoracionDao;

public class ValoracionRepository {
    private static final String LOG_TAG = ComentarioRepository.class.getSimpleName();

    private static ValoracionRepository sInstance;
    private final ValoracionDao mValoracionDao;
    private final MutableLiveData<Long> mValoracionLiveData = new MutableLiveData<>();

    private ValoracionRepository (ValoracionDao valoracionDao) {
        mValoracionDao = valoracionDao;
    }
    public synchronized static ValoracionRepository getInstance(ValoracionDao  valoracionDao) {
        Log.i(LOG_TAG, "Getting the ValoracionRepository");
        if (sInstance == null) {
            sInstance = new ValoracionRepository(valoracionDao);
            Log.i(LOG_TAG, "Made the instance of the Valoracion Repository");
        }
        return sInstance;
    }

    public void setLocalId (long id) {
        mValoracionLiveData.setValue(id);
    }

    public LiveData<Valoracion> getValoracion () {
        Log.i(LOG_TAG, "Fetching Valoracion from data base");
        return Transformations.switchMap(mValoracionLiveData, idLocal -> mValoracionDao.get(idLocal));
    }
}
