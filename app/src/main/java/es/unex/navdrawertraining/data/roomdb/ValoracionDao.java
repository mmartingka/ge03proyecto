package es.unex.navdrawertraining.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import es.unex.navdrawertraining.data.modelodatos.Valoracion;

@Dao
public interface ValoracionDao {
    @Query("SELECT * FROM Valoracion WHERE id_valoracion = :id")
    public LiveData<Valoracion> get(long id);

    @Insert
    public long insert(Valoracion valoracion);

    @Update
    public int update(Valoracion valoracion);
}
