package es.unex.navdrawertraining.data.modelodatos;

import android.os.Bundle;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity(tableName="Categoria")
public class Categoria {

    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public final static String ID_CATEGORIA = "id_categoria";
    @Ignore
    public final static String NOMBRE_CATEGORIA = "nombre_categoria";
    @Ignore
    public final static String ICONO_CATEGORIA = "icono_categoria";
    @Ignore
    public final static String COLOR_CATEGORIA = "color_categoria";

    @PrimaryKey(autoGenerate = true)
    private long idCat;
    @ColumnInfo(name = "nombre_categoria")
    private String nombre_categoria;
    @ColumnInfo(name = "icono_categoria")
    private String icono_categoria;
    @ColumnInfo(name = "color_categoria")
    private String color_categoria;

    @Ignore
    public Categoria() {
        this.nombre_categoria = null;
        this.icono_categoria = null;
        this.color_categoria = null;
    }

    public Categoria(long idCat, String nombre_categoria, String icono_categoria, String color_categoria) {
        this.idCat = idCat;
        this.nombre_categoria = nombre_categoria;
        this.icono_categoria = icono_categoria;
        this.color_categoria = color_categoria;
    }

    public long getIdCat() {
        return idCat;
    }

    public void setIdCat(long idCat) {
        this.idCat = idCat;
    }

    public String getNombre_categoria() {
        return nombre_categoria;
    }

    public void setNombre_categoria(String nombre_categoria) {
        this.nombre_categoria = nombre_categoria;
    }

    public String getIcono_categoria() {
        return icono_categoria;
    }

    public void setIcono_categoria(String icono_categoria) {
        this.icono_categoria = icono_categoria;
    }

    public String getColor_categoria() {
        return color_categoria;
    }

    public void setColor_categoria(String color_categoria) {
        this.color_categoria = color_categoria;
    }

    public Bundle bundlelizeCategoria () {
        Bundle bundle = new Bundle();
        bundle.putLong("id", idCat);
        return bundle;
    }
}
