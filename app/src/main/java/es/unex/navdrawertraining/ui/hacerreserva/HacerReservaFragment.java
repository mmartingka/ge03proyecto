package es.unex.navdrawertraining.ui.hacerreserva;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import java.util.Calendar;

import es.unex.navdrawertraining.AppExecutors;
import es.unex.navdrawertraining.databinding.FragmentHacerReservaBinding;
import es.unex.navdrawertraining.data.modelodatos.Reserva;
import es.unex.navdrawertraining.data.roomdb.CeresLimitDatabase;
import es.unex.navdrawertraining.data.roomdb.DateConverter;


public class HacerReservaFragment extends Fragment {
    private FragmentHacerReservaBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentHacerReservaBinding.inflate(inflater, container, false);

        //Name of the local
        TextView nombre = binding.tvNombreLocal;
        nombre.setText(getArguments().getString("nombre"));

        //Background and profile images
        Picasso.get().load(getArguments().getString("fotoBackground")).into(binding.ivBackgroundLocal);
        Picasso.get().load(getArguments().getString("fotoPerfil")).into(binding.ivLogoLocal);

        //Listener to select the date
        EditText fecha = binding.etFecha;
        fecha.setOnClickListener(view -> showDatePickerDialog());
        //Listener to select the time
        EditText hora = binding.etHora;
        hora.setOnClickListener(view -> showTimePickerDialog());

        //Listener to confirm the reserve
        binding.bHacerReservaConfirmar.setOnClickListener(view -> {
            if( !nombre.getText().toString().isEmpty() && !fecha.getText().toString().isEmpty() && !hora.getText().toString().isEmpty()) {
                Calendar calendar = Calendar.getInstance();
                String[] fecha1 = binding.etFecha.getText().toString().split("/");
                String[] hora1 = binding.etHora.getText().toString().split(":");


                calendar.set(Integer.valueOf(fecha1[2]), Integer.valueOf(fecha1[1]), Integer.valueOf(fecha1[0]), Integer.valueOf(hora1[0]), Integer.valueOf(hora1[1]), 0);
                calendar.set(Calendar.MILLISECOND, 0);
                if (calendar.getTimeInMillis() < (System.currentTimeMillis() + 1800000.0f)) {
                    Toast.makeText(view.getContext(),
                            "La reserva tiene que ser, como poco, con media hora de antelación.",
                            Toast.LENGTH_LONG).show();
                }else {
                    Reserva reserva = new Reserva(getArguments().getLong("id"), calendar.getTime());

                    AppExecutors.getInstance().diskIO().execute(() -> {
                        Reserva bdReserva = CeresLimitDatabase.getInstance(getContext()).getDaoReserva().get(reserva.getId_reserva(), DateConverter.toTimestamp(reserva.getFecha_hora_reserva()));
                        if (bdReserva == null) {
                            CeresLimitDatabase.getInstance(getContext()).getDaoReserva().insert(reserva);
                            getActivity().runOnUiThread(() -> getActivity().onBackPressed());
                        } else {
                            getActivity().runOnUiThread(() -> Toast.makeText(view.getContext(),
                                    "Ya tienes una reserva en este local a esta hora en este día!.",
                                    Toast.LENGTH_LONG).show());
                        }
                    });
                }
            }
        });

        return binding.getRoot();
    }
    private void showDatePickerDialog() {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }
    private void showTimePickerDialog() {
        TimePickerFragment newFragment = new TimePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}