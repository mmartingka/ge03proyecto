package es.unex.navdrawertraining.ui.categorias;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.R;
import es.unex.navdrawertraining.databinding.FragmentCategoriasBinding;

public class CategoriasFragment extends Fragment{
    private FragmentCategoriasBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentCategoriasBinding.inflate(inflater, container, false);

        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the CategoriaViewModel that outlives the Fragment lifecyle
        CategoriaViewModel categoriaViewModel = new ViewModelProvider(this, diContainer.mCategoriaViewModelFactory).get(CategoriaViewModel.class);

        //Prepare the RecyclerView containing Categorias
        RecyclerView recyclerView = binding.rvCategorias;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        CategoriasAdapter mAdapter = new CategoriasAdapter(getContext());
        recyclerView.setAdapter(mAdapter);

        //Observe changes over the LiveData field, containing Categoria items, of CategoriaViewModel
        categoriaViewModel.getCategorias().observe(this.getViewLifecycleOwner(), categorias -> mAdapter.swap(categorias));

        //Listener to create a new category
        binding.fbCrearCategoria.setOnClickListener(view -> Navigation.findNavController(view).navigate(R.id.crear_categoria_action));

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}