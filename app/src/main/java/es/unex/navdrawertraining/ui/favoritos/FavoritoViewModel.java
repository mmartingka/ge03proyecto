package es.unex.navdrawertraining.ui.favoritos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.navdrawertraining.data.repository.FavoritoRepository;
import es.unex.navdrawertraining.data.modelodatos.Local;

public class FavoritoViewModel extends ViewModel {
    private final LiveData<List<Local>> favoritos;

    public FavoritoViewModel(FavoritoRepository favoritoRepository) {
        this.favoritos = favoritoRepository.getLocalesFavoritos();
    }

    public LiveData<List<Local>> getFavoritos() {
        return favoritos;
    }
}
