package es.unex.navdrawertraining.ui.home;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.navdrawertraining.data.repository.CategoriaRepository;
import es.unex.navdrawertraining.data.repository.LocalRepository;

public class HomeViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final LocalRepository mLocalRepository;
    private final CategoriaRepository mCategoriaRepository;

    public HomeViewModelFactory(LocalRepository mLocalRepository, CategoriaRepository mCategoriaRepository) {
        this.mLocalRepository = mLocalRepository;
        this.mCategoriaRepository = mCategoriaRepository;
    }
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HomeViewModel(mLocalRepository, mCategoriaRepository);
    }
}
