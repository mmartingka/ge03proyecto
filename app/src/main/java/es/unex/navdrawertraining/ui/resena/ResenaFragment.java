package es.unex.navdrawertraining.ui.resena;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;

import com.squareup.picasso.Picasso;

import java.util.concurrent.Executor;

import es.unex.navdrawertraining.AppExecutors;
import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.databinding.FragmentResenaBinding;
import es.unex.navdrawertraining.data.modelodatos.Comentario;
import es.unex.navdrawertraining.data.modelodatos.Valoracion;
import es.unex.navdrawertraining.data.roomdb.CeresLimitDatabase;
import es.unex.navdrawertraining.ui.detalles.DetallesViewModel;


public class ResenaFragment extends Fragment{
    private FragmentResenaBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentResenaBinding.inflate(inflater, container, false);

        //Name of the local
        binding.tvNombreLocal.setText(getArguments().getString("nombre"));

        //Background and profile images
        Picasso.get().load(getArguments().getString("fotoPerfil")).into(binding.ivLogoLocal);
        Picasso.get().load(getArguments().getString("fotoBackground")).into(binding.ivBackgroundLocal);

        //Ratingbar management
        RatingBar ratingBar = binding.ratingbarLocal;
        ratingBar.setOnRatingBarChangeListener((ratingBar1, rating, fromUser) -> { if (rating == 0.0f) ratingBar1.setRating(0.5f); });

        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the DetallesViewModel that outlives the Fragment lifecyle
        DetallesViewModel detallesViewModel = new ViewModelProvider(this, diContainer.mDetallesViewModelFactory).get(DetallesViewModel.class);

        //Set the id of the Local and observe changes in the LiveData fields, containing the rating and the comment, of DetallesViewModel
        detallesViewModel.setId_local(getArguments().getLong("id"));

        detallesViewModel.getValoracion().observe(this.getViewLifecycleOwner(), valoracion -> { if (valoracion != null) binding.ratingbarLocal.setRating(valoracion.getPuntuacion());});
        detallesViewModel.getComentario().observe(this.getViewLifecycleOwner(), comentario -> { if (comentario != null) binding.etComentario.setText(comentario.getCadena_comentario()); });

        EditText etComentario = binding.etComentario;
        binding.bConfirmar.setOnClickListener(view -> {
                Executor disk = AppExecutors.getInstance().diskIO();
                detallesViewModel.getValoracion().observe(ResenaFragment.this.getViewLifecycleOwner(), valoracion -> {
                    final Valoracion fval;
                    if (valoracion == null && ratingBar.getRating() > 0.0f) { //By default, the minimun rating is 0.5. If it is 0, then it means that there was no vote
                        fval = new Valoracion(getArguments().getLong("id"), ratingBar.getRating());
                        disk.execute(() -> CeresLimitDatabase.getInstance(getContext()).getDaoValoracion().insert(fval));
                    } else {
                        if (ratingBar.getRating() > 0.0f) {
                            valoracion.setPuntuacion(ratingBar.getRating());
                            fval = valoracion;
                            disk.execute(() -> CeresLimitDatabase.getInstance(getContext()).getDaoValoracion().update(fval));
                        }
                    }
                });
                detallesViewModel.getComentario().observe(ResenaFragment.this.getViewLifecycleOwner(), comentario -> {
                    final Comentario fcoment;
                    if (!etComentario.getText().toString().isEmpty()){
                        if (comentario == null) {
                            fcoment = new Comentario(getArguments().getLong("id"), etComentario.getText().toString());
                            disk.execute(() -> CeresLimitDatabase.getInstance(getContext()).getDaoComentario().insert(fcoment));
                        } else {
                            comentario.setCadena_comentario(etComentario.getText().toString());
                            fcoment= comentario;
                            disk.execute(() -> CeresLimitDatabase.getInstance(getContext()).getDaoComentario().update(fcoment));
                        }
                    }
                });

            getActivity().onBackPressed();
        });

        return binding.getRoot();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}