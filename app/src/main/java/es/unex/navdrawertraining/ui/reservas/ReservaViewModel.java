package es.unex.navdrawertraining.ui.reservas;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.navdrawertraining.data.repository.ReservaRepository;
import es.unex.navdrawertraining.helpers.LocalReservaAggregate;

public class ReservaViewModel extends ViewModel {
    private final LiveData<List<LocalReservaAggregate>> reservas;

    public ReservaViewModel (ReservaRepository reservaRepository) {
        reservas = reservaRepository.getReservas();
    }

    public LiveData<List<LocalReservaAggregate>> getReservas() {
        return reservas;
    }
}
