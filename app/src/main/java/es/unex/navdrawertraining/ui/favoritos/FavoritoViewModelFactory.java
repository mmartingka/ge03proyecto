package es.unex.navdrawertraining.ui.favoritos;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.navdrawertraining.data.repository.FavoritoRepository;

public class FavoritoViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final FavoritoRepository mFavoritoRepository;

    public FavoritoViewModelFactory (FavoritoRepository mFavoritoRepository) {
        this.mFavoritoRepository = mFavoritoRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new FavoritoViewModel(mFavoritoRepository);
    }
}
