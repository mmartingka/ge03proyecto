package es.unex.navdrawertraining.ui.detalles;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.navdrawertraining.data.repository.ComentarioRepository;
import es.unex.navdrawertraining.data.repository.FavoritoRepository;
import es.unex.navdrawertraining.data.repository.ValoracionRepository;

public class DetallesViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final ComentarioRepository mComentarioRepository;
    private final ValoracionRepository mValoracionRepository;
    private final FavoritoRepository mFavoritoRepository;

    public DetallesViewModelFactory(ComentarioRepository mComentarioRepository, ValoracionRepository mValoracionRepository, FavoritoRepository mFavoritoRepository) {
        this.mComentarioRepository = mComentarioRepository;
        this.mValoracionRepository = mValoracionRepository;
        this.mFavoritoRepository = mFavoritoRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new DetallesViewModel(mComentarioRepository, mValoracionRepository, mFavoritoRepository);
    }
}
