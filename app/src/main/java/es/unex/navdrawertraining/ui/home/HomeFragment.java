package es.unex.navdrawertraining.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);

        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the HomeViewModel that outlives the Fragment lifecyle
        HomeViewModel homeViewModel = new ViewModelProvider(this, diContainer.mHomeViewModelFactory).get(HomeViewModel.class);

        //Prepare the RecyclerView containing Locales
        RecyclerView rvRecomendaciones = binding.rvHomeRecomendacionesList;
        rvRecomendaciones.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        HomeLocalesAdapter localesAdapter = new HomeLocalesAdapter();
        rvRecomendaciones.setAdapter(localesAdapter);

        //Prepare the RecyclerView containing Categorias
        RecyclerView rvCategorias = binding.rvHomeCategoriasList;
        rvCategorias.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        HomeCategoriasAdapter categoriasAdapter = new HomeCategoriasAdapter(getContext());
        rvCategorias.setAdapter(categoriasAdapter);

        //Observe changes over the LiveData fields, containing Categoria and Local items, of HomeViewModel
        homeViewModel.getLocales().observe(this.getViewLifecycleOwner(), locales -> localesAdapter.swap(locales));
        homeViewModel.getCategorias().observe(this.getViewLifecycleOwner(), categorias -> categoriasAdapter.swap(categorias));

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}