package es.unex.navdrawertraining.ui.reservas;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.navdrawertraining.data.repository.ReservaRepository;

public class ReservaViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final ReservaRepository mReservaRepository;

    public ReservaViewModelFactory(ReservaRepository mReservaRepository) {
        this.mReservaRepository = mReservaRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ReservaViewModel(mReservaRepository);
    }
}
