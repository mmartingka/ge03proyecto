package es.unex.navdrawertraining.ui.categorias;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.unex.navdrawertraining.AppExecutors;
import es.unex.navdrawertraining.R;
import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.roomdb.CeresLimitDatabase;

public class LocalesCategoriaAdapter extends RecyclerView.Adapter<LocalesCategoriaAdapter.ViewHolder> {
    private List<Local> locales;
    private final long categoriaId;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nombre;
        private final ImageView background;
        private final ImageView icono;
        private final RatingBar rating;
        private final ImageButton eliminarLocal;
        private final CardView container;

        public ViewHolder(@NonNull View view) {
            super(view);
            nombre = (TextView)  view.findViewById(R.id.tv_localesCategoria_nombre_item);
            background = (ImageView) view.findViewById(R.id.iv_localesCategoria_background_local);
            icono = (ImageView) view.findViewById(R.id.iv_localesCategoria_imagen_local);
            rating = (RatingBar) view.findViewById(R.id.rv_localesCategorias_rating);
            eliminarLocal = (ImageButton) view.findViewById(R.id.ib_localesCategorias_eliminar);
            container = (CardView) view.findViewById(R.id.cardview_localesCategoria_item);
        }

        public TextView getNombre() {
            return nombre;
        }

        public ImageView getBackground() {
            return background;
        }

        public ImageView getIcono() {
            return icono;
        }

        public RatingBar getRating() {
            return rating;
        }

        public ImageButton getEliminarLocal() {
            return eliminarLocal;
        }

        public CardView getContainer() {
            return container;
        }
    }

    public LocalesCategoriaAdapter(long categoriaId) {
        locales = new ArrayList<Local>();
        this.categoriaId = categoriaId;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_locales_categoria_item_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.getNombre().setText(locales.get(holder.getAdapterPosition()).getNombre());
        holder.getRating().setRating(locales.get(holder.getAdapterPosition()).getValoracionGlobal());
        Picasso.get().load(locales.get(holder.getAdapterPosition()).getFotoPerfil()).into(holder.icono);
        Picasso.get().load(locales.get(holder.getAdapterPosition()).getFotoBackground()).into(holder.background);

        holder.getEliminarLocal().setOnClickListener(view -> AppExecutors.getInstance().diskIO().execute(() -> {
            CeresLimitDatabase.getInstance(view.getContext()).getDaoLocalCategoria().delete(categoriaId, locales.get(holder.getAdapterPosition()).getId());
            locales.remove(holder.getAdapterPosition());
            AppExecutors.getInstance().mainThread().execute(() -> notifyDataSetChanged());
        }));

        final Local finalLocal = locales.get(holder.getAdapterPosition());
        holder.getContainer().setOnClickListener(view -> Navigation.findNavController(view).navigate(R.id.local_categoria_to_detalles_action, finalLocal.bundlelizeLocal()));
    }

    @Override
    public int getItemCount() {
        return locales.size();
    }

    public void swap(List<Local> locales) {
        this.locales = locales;
        notifyDataSetChanged();

    }
}
