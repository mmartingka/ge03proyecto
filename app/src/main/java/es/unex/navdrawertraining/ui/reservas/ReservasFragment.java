package es.unex.navdrawertraining.ui.reservas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.databinding.FragmentReservasBinding;


public class ReservasFragment extends Fragment{
    private FragmentReservasBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentReservasBinding.inflate(inflater, container, false);

        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the ReservaViewModel that outlives the Fragment lifecyle
        ReservaViewModel reservaViewModel = new ViewModelProvider(this, diContainer.mReservaViewModelFactory).get(ReservaViewModel.class);

        //Prepare the RecyclerView containing Reservas
        RecyclerView recyclerView = binding.rvReservas;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        ReservasAdapter reservasAdapter = new ReservasAdapter();
        recyclerView.setAdapter(reservasAdapter);

        //Observe changes over the LiveData field, containing LocalReservaAggregate items, of ReservaViewModel
        reservaViewModel.getReservas().observe(this.getViewLifecycleOwner(), reservas -> reservasAdapter.swap(reservas));

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


}