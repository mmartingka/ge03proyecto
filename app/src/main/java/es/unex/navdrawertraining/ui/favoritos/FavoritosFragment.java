package es.unex.navdrawertraining.ui.favoritos;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.databinding.FragmentFavoritosBinding;


public class FavoritosFragment extends Fragment{
    private FragmentFavoritosBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentFavoritosBinding.inflate(inflater, container, false);

        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the FavoritoViewModel that outlives the Fragment lifecyle
        FavoritoViewModel favoritoViewModel = new ViewModelProvider(this, diContainer.mFavoritoViewModelFactory).get(FavoritoViewModel.class);

        //Prepare the RecyclerView containing the favourite Locales
        RecyclerView recyclerView = binding.rvCategorias;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        FavoritosAdapter mFavoritosAdapter = new FavoritosAdapter();
        recyclerView.setAdapter(mFavoritosAdapter);

        //Observe changes over the LiveData field, containing Local items, of FavoritoViewModel
        favoritoViewModel.getFavoritos().observe(this.getViewLifecycleOwner(), favoritos -> { if (favoritos != null) { mFavoritosAdapter.swap(favoritos); } });

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}