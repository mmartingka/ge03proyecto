package es.unex.navdrawertraining.ui.categorias;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.databinding.FragmentLocalesCategoriaBinding;

public class LocalesCategoriaFragment extends Fragment {
    private FragmentLocalesCategoriaBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentLocalesCategoriaBinding.inflate(inflater, container, false);

        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the LocalesCategoriaViewModel that outlives the Fragment lifecyle
        LocalesCategoriaViewModel localesCategoriaViewModel = new ViewModelProvider(this, diContainer.mLocalesCategoriaViewModelFactory).get(LocalesCategoriaViewModel.class);

        RecyclerView recyclerView = binding.rvLocalescategorias;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        LocalesCategoriaAdapter mAdapter = new LocalesCategoriaAdapter(getArguments().getLong("id"));
        recyclerView.setAdapter(mAdapter);

        //Observe changes over the LiveData field, containing Local items, of LocalesCategoriaViewModel
        localesCategoriaViewModel.setIdCategoria(getArguments().getLong("id"));
        localesCategoriaViewModel.getLocales().observe(this.getViewLifecycleOwner(), locales -> mAdapter.swap(locales));

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}