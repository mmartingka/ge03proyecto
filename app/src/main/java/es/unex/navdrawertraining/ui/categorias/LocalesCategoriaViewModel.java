package es.unex.navdrawertraining.ui.categorias;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.navdrawertraining.data.repository.LocalCategoriaRepository;
import es.unex.navdrawertraining.data.modelodatos.Local;

public class LocalesCategoriaViewModel extends ViewModel {
    private final LocalCategoriaRepository mLocalCategoriaRepository;
    private final LiveData<List<Local>> locales;

    public LocalesCategoriaViewModel (LocalCategoriaRepository localCategoriaRepository) {
        mLocalCategoriaRepository = localCategoriaRepository;
        locales = mLocalCategoriaRepository.getLocalesCategoria();
    }

    public LiveData<List<Local>> getLocales() {
        return locales;
    }
    public void setIdCategoria (long idCategoria) {
        mLocalCategoriaRepository.setCategoriaId(idCategoria);
    }
}
