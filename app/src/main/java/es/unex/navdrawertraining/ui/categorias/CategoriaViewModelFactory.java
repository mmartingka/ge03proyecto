package es.unex.navdrawertraining.ui.categorias;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.navdrawertraining.data.repository.CategoriaRepository;

public class CategoriaViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final CategoriaRepository mCategoriaRepository;


    public CategoriaViewModelFactory(CategoriaRepository categoriaRepository) {
        mCategoriaRepository = categoriaRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CategoriaViewModel(mCategoriaRepository);
    }
}
