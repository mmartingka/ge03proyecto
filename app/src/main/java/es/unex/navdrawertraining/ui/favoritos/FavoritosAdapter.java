package es.unex.navdrawertraining.ui.favoritos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.unex.navdrawertraining.AppExecutors;
import es.unex.navdrawertraining.R;
import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.roomdb.CeresLimitDatabase;

public class FavoritosAdapter extends RecyclerView.Adapter<FavoritosAdapter.ViewHolder>{
    List<Local> locales;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nombre;
        private final ImageView background;
        private final ImageView icono;
        private final RatingBar rating;
        private final ImageButton eliminarFavorito;
        private final CardView container;

        public ViewHolder(@NonNull View view) {
            super(view);
            nombre = (TextView)  view.findViewById(R.id.tv_favoritos_nombre_item);
            background = (ImageView) view.findViewById(R.id.iv_favoritos_background_local);
            icono = (ImageView) view.findViewById(R.id.iv_favoritos_imagen_local);
            rating = (RatingBar) view.findViewById(R.id.rv_favoritos_rating);
            eliminarFavorito = (ImageButton) view.findViewById(R.id.ib_favoritos_detalles_favorito);
            container = (CardView) view.findViewById(R.id.cardview_favoritos_item);

        }

        public TextView getNombre() {
            return nombre;
        }

        public ImageView getBackground() {
            return background;
        }

        public ImageView getIcono() {
            return icono;
        }

        public RatingBar getRating() {
            return rating;
        }

        public ImageButton getEliminarFavorito() {
            return eliminarFavorito;
        }

        public CardView getContainer() {
            return container;
        }
    }

    public FavoritosAdapter(){
        locales = new ArrayList<Local>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_favoritos_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.getNombre().setText(locales.get(holder.getAdapterPosition()).getNombre());
        holder.getRating().setRating(locales.get(holder.getAdapterPosition()).getValoracionGlobal());
        Picasso.get().load(locales.get(holder.getAdapterPosition()).getFotoPerfil()).into(holder.icono);
        Picasso.get().load(locales.get(holder.getAdapterPosition()).getFotoBackground()).into(holder.background);

        holder.getEliminarFavorito().setOnClickListener(view -> AppExecutors.getInstance().diskIO().execute(() -> {
            CeresLimitDatabase.getInstance(view.getContext()).getDaoFavorito().delete(locales.get(holder.getAdapterPosition()).getId());
            locales.remove(holder.getAdapterPosition());
            AppExecutors.getInstance().mainThread().execute(() -> notifyDataSetChanged());
        }));

        final Local finalLocal = locales.get(holder.getAdapterPosition());
        holder.getContainer().setOnClickListener(view -> Navigation.findNavController(view).navigate(R.id.fav_to_detalles_action, finalLocal.bundlelizeLocal()));

    }

    @Override
    public int getItemCount() {
        return locales.size();
    }

    public void swap(List<Local> locales) {
        this.locales = locales;
        notifyDataSetChanged();

    }
}
