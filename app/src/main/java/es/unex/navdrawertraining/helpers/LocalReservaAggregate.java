package es.unex.navdrawertraining.helpers;

import androidx.room.Embedded;

import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.modelodatos.Reserva;

public class LocalReservaAggregate {
    @Embedded
    private final Local local;
    @Embedded
    private final Reserva reserva;

    public LocalReservaAggregate(Local local, Reserva reserva) {
        this.local = local;
        this.reserva = reserva;
    }

    public Local getLocal() {
        return local;
    }

    public Reserva getReserva() {
        return reserva;
    }
}
