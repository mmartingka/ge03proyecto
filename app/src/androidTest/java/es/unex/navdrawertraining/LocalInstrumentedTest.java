package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;

import android.os.Bundle;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Local;

@RunWith(AndroidJUnit4.class)
public class LocalInstrumentedTest {

    @Test
    public void bundlelizeLocaTest() throws NoSuchFieldException, IllegalAccessException {
        final Local instance = new Local();
        final Field id = instance.getClass().getDeclaredField("id");
        id.setAccessible(true);
        id.set(instance, 1L);

        final Field nombre = instance.getClass().getDeclaredField("nombre");
        nombre.setAccessible(true);
        nombre.set(instance, "Urban's");

        final Field descripcion = instance.getClass().getDeclaredField("descripcion");
        descripcion.setAccessible(true);
        descripcion.set(instance, "Buen sitio");

        final Field coordenadas = instance.getClass().getDeclaredField("coordenadas");
        coordenadas.setAccessible(true);
        List<Double> coord = new ArrayList<>();
        coord.add(5.34);
        coord.add(4.32);
        coordenadas.set(instance, coord);

        final Field valoracionGlobal = instance.getClass().getDeclaredField("valoracionGlobal");
        valoracionGlobal.setAccessible(true);
        valoracionGlobal.set(instance, 4.5F);

        final Field fotoPerfil = instance.getClass().getDeclaredField("fotoPerfil");
        fotoPerfil.setAccessible(true);
        fotoPerfil.set(instance, "fotoPerfil");

        final Field fotoBackground = instance.getClass().getDeclaredField("fotoBackground");
        fotoBackground.setAccessible(true);
        fotoBackground.set(instance, "fotoBackground");

        final Field aforoTotal = instance.getClass().getDeclaredField("aforoTotal");
        aforoTotal.setAccessible(true);
        aforoTotal.set(instance, 100);

        final Field ocupacionActual = instance.getClass().getDeclaredField("ocupacionActual");
        ocupacionActual.setAccessible(true);
        ocupacionActual.set(instance, 50);

        final Field etiquetas = instance.getClass().getDeclaredField("etiquetas");
        etiquetas.setAccessible(true);
        List<String> tags = new ArrayList<String>();
        tags.add("Cafetería");
        tags.add("Rastaurante");
        etiquetas.set(instance, tags);

        Bundle bundle = instance.bundlelizeLocal();

        assertEquals("The bundle does not match the fields of the instance", 1L, bundle.getLong("id"));
        assertEquals("The bundle does not match the fields of the instance", bundle.getString("nombre"), "Urban's");
        assertEquals("The bundle does not match the fields of the instance", bundle.getString("descripcion"), "Buen sitio");
        assertEquals("The bundle does not match the fields of the instance", bundle.getDoubleArray("coordenadas")[0], coord.get(0), 0.001F);
        assertEquals("The bundle does not match the fields of the instance", bundle.getDoubleArray("coordenadas")[1], coord.get(1), 0.001F);
        assertEquals("The bundle does not match the fields of the instance", bundle.getFloat("valoracion"), 4.5F, 0.001F);
        assertEquals("The bundle does not match the fields of the instance", bundle.getString("fotoPerfil"), "fotoPerfil");
        assertEquals("The bundle does not match the fields of the instance", bundle.getString("fotoBackground"), "fotoBackground");
        assertEquals("The bundle does not match the fields of the instance", bundle.getInt("aforoTotal"), 100);
        assertEquals("The bundle does not match the fields of the instance", bundle.getInt("ocupacionActual"), 50);
        assertEquals("The bundle does not match the fields of the instance", bundle.getStringArrayList("tags"), tags);
    }
}
