package es.unex.navdrawertraining;


import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import static androidx.test.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.assertion.ViewAssertions.*;
import static androidx.test.espresso.matcher.ViewMatchers.*;

import es.unex.navdrawertraining.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class VerInformacionLocalTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void verInformacionLocalTest() {
        ViewInteraction imageButton = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(allOf(withId(R.id.toolbar),
                                withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class)))),
                        isDisplayed()));
        imageButton.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction checkedTextView = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Búsqueda Avanzada"),
                        withParent(allOf(withId(R.id.nav_busquedaAvanzada),
                                withParent(withId(R.id.design_navigation_view)))),
                        isDisplayed()));
        checkedTextView.check(matches(isDisplayed()));

        ViewInteraction navigationMenuItemView = onView(
                allOf(withId(R.id.nav_busquedaAvanzada),
                        childAtPosition(
                                allOf(withId(R.id.design_navigation_view),
                                        childAtPosition(
                                                withId(R.id.nav_view),
                                                0)),
                                2),
                        isDisplayed()));
        navigationMenuItemView.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.tv_home_item), withText("Urban's"),
                        withParent(allOf(withId(R.id.cardview_item),
                                withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class)))),
                        isDisplayed()));
        textView.check(matches(withText("Urban's")));

        ViewInteraction cardView = onView(
                allOf(withId(R.id.cardview_item),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rv_busquedaAvanzada),
                                        0),
                                0),
                        isDisplayed()));
        cardView.perform(click());

        ViewInteraction imageView3 = onView(
                allOf(withId(R.id.iv_backgroundLocal), isDisplayed()));
        imageView3.check(matches(isDisplayed()));

        ViewInteraction imageButton2 = onView(
                allOf(withId(R.id.ib_detalles_favorito), withContentDescription("Añadir o quitar local de favoritos"),
                        isDisplayed()));
        imageButton2.check(matches(isDisplayed()));

        ViewInteraction imageView4 = onView(
                allOf(withId(R.id.iv_logoLocal),
                        isDisplayed()));
        imageView4.check(matches(isDisplayed()));

        ViewInteraction imageButton3 = onView(
                allOf(withId(R.id.ib_detalles_categoria), withContentDescription("Añadir o quitar local de favoritos"),
                        isDisplayed()));
        imageButton3.check(matches(isDisplayed()));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.tv_nombreLocal), withText("Urban's"), isDisplayed()));
        textView3.check(matches(withText("Urban's")));

        ViewInteraction ratingBar2 = onView(
                allOf(withId(R.id.ratingbar_local), withContentDescription("Valoración del local"), isDisplayed()));
        ratingBar2.check(matches(isDisplayed()));

        ViewInteraction textView5 = onView(
                allOf(withId(R.id.tv_aforo), withText("Aforo"), isDisplayed()));
        textView5.check(matches(withText("Aforo")));

        ViewInteraction progressBar = onView(
                allOf(withId(R.id.progress_bar), isDisplayed()));
        progressBar.check(matches(isDisplayed()));

        ViewInteraction textView6 = onView(
                allOf(withId(R.id.tv_detalles_numaforo), withText("40 / 100"), isDisplayed()));
        textView6.check(matches(withText("40 / 100")));

        onView(ViewMatchers.withId(R.id.swipeRlayout)).perform(ViewActions.swipeUp());

        ViewInteraction button = onView(
                allOf(withId(R.id.b_resena), withText("DEJAR UNA RESEÑA"), isDisplayed()));
        button.check(matches(isDisplayed()));

        ViewInteraction imageButton4 = onView(
                allOf(withId(R.id.fb_reservar), withContentDescription("Realizar una reserva"),
                        withParent(withParent(withId(R.id.swipeRlayout))),
                        isDisplayed()));
        imageButton4.check(matches(isDisplayed()));

        ViewInteraction textView7 = onView(
                allOf(withId(R.id.tv_titulo_desc), withText("Descripción"), isDisplayed()));
        textView7.check(matches(withText("Descripción")));

        ViewInteraction textView8 = onView(
                allOf(withId(R.id.tv_descipcion), withText("Este establecimiento es un bar de tapas y raciones con muy buenos desayunos. Está en un barrio tranquilo y familiar y tiene un parque al lado. La terraza es muy grande y da mucho sol.")));
        textView8.check(matches(withText("Este establecimiento es un bar de tapas y raciones con muy buenos desayunos. Está en un barrio tranquilo y familiar y tiene un parque al lado. La terraza es muy grande y da mucho sol.")));

        ViewInteraction textView9 = onView(
                allOf(withId(R.id.tv_titulo_comen), withText("Comentarios")));
        textView9.check(matches(withText("Comentarios")));

        ViewInteraction textView10 = onView(
                allOf(withId(R.id.tv_detalles_5mentarios), withText("¡No has hecho ningún comentario aún!")));
        textView10.check(matches(withText("¡No has hecho ningún comentario aún!")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
